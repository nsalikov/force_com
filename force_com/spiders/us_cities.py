# -*- coding: utf-8 -*-
import re
import csv
import json
import time
import scrapy

from scrapy.shell import inspect_response

from ..scrapy_selenium import SeleniumRequest
from selenium.webdriver.common.by import By
from selenium.webdriver.common.alert import Alert
from selenium.webdriver.support.ui import Select
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC


class UsCitiesSpider(scrapy.Spider):
    name = 'us_cities'
    allowed_domains = ['dell.force.com']
    start_urls = ['http://dell.force.com/FAP_PartnerSearch']

    take_screenshot = False
    cities = []


    def start_requests(self):
        if 'ZIPCODES' not in self.settings:
            raise CloseSpider("You must specificy ZIPCODES file in settings.py")

        with open(self.settings['ZIPCODES'], encoding='utf8') as csvfile:
            dr = csv.DictReader(csvfile)
            # next(dr)
            for line in dr:
                self.cities.append(line['City'].strip())

        for url in self.start_urls:
            yield SeleniumRequest(url=url, callback=self.parse, wait_time=15, wait_until=EC.presence_of_element_located((By.CSS_SELECTOR, '[id="thePage:theForm:resultsTable"]')))


    def parse(self, response):
        # inspect_response(response, self)

        self.check_page(response)
        driver = response.meta['driver']

        for city in self.cities:
            self.change_city(response, city)
            yield from self.parse_partner(response, city)


    def change_city(self, response, city):
        driver = response.meta['driver']

        try:
            select = Select(driver.find_element_by_css_selector('select[id="thePage:theForm:searchComponent:searchFilterComponent:repeat:0:countryCode2"]'))
            cc = select.first_selected_option.get_attribute('value')
            if cc != 'US':
                self.logger.info('Changing country to: {}'.format('US'))
                select.select_by_value('US')
                self.check_page(response)
        except Exception as e:
            self.logger.error('Something wrong', exc_info=True)
            return

        try:
            self.logger.info('Changing city to: {}'.format(city))
            element = driver.find_element_by_css_selector('input[id="thePage:theForm:searchComponent:searchFilterComponent:repeat:0:txtLocation3"]')
            element.clear()
            element.send_keys(city)
        except Exception as e:
            self.logger.error('Something wrong', exc_info=True)
            return

        try:
            select = Select(driver.find_element_by_css_selector('select[id="thePage:theForm:searchComponent:searchFilterComponent:repeat:0:ddlDistance4"]'))
            cc = select.first_selected_option.get_attribute('value')
            if cc != '200':
                self.logger.info('Changing distance to: {}'.format('200'))
                select.select_by_value('200')
                self.check_page(response)
        except Exception as e:
            self.logger.error('Something wrong', exc_info=True)
            return

        button = driver.find_element_by_css_selector('input[id="thePage:theForm:searchComponent:searchFilterComponent:repeat:0:searchicon3"]')
        button.click()

        self.check_page(response)
        time.sleep(5)

        return self.check_cities(response, city)


    def parse_partner(self, response, city, timeout=5):
        driver = response.meta['driver']

        try:
            select = Select(driver.find_element_by_css_selector('select[id="thePage:theForm:searchComponent:searchFilterComponent:repeat:0:countryCode2"]'))
            cc = select.first_selected_option.get_attribute('value')
        except Exception as e:
            self.logger.error('Something wrong', exc_info=True)
            return

        while True:
            current_response = response.replace(body=str.encode(driver.page_source))
            partners = current_response.css('input[id="thePage:theForm:LocationData"]::attr(value)').extract_first()
            partners = partners.replace(r'\\"', r'\"')

            try:
                partners = json.loads(partners)
            except Exception as e:
                self.logger.error('Unable to parse city data: {}'.format(city), exc_info=True)
                self.logger.debug('{}'.format(partners))
                return

            partners = [{'url': 'http://dell.force.com/FAP_PartnerDetails?id={}'.format(p['id'].strip()), 'country': p['country'].strip(), 'city': city} for p in partners['locations']]

            yield from partners

            try:
                next_page = WebDriverWait(driver, timeout).until(EC.presence_of_element_located((By.CSS_SELECTOR, 'span[id="thePage:theForm:pageprev"] input[type="submit"]')))
                next_page = WebDriverWait(driver, timeout).until(EC.element_to_be_clickable((By.CSS_SELECTOR, 'span[id="thePage:theForm:pageprev"] input[type="submit"]')))
                driver.execute_script("return arguments[0].scrollIntoView(false);", next_page)
                driver.execute_script("window.scrollBy(0, 200);")
                next_page.click()
            except:
                self.logger.debug('Breaking cycle', exc_info=True)
                if self.take_screenshot:
                    filename =  cc + '.png'
                    driver.save_screenshot(filename)
                break
            else:
                self.check_page(response)


    def check_page(self, response, timeout=5):
        driver = response.meta['driver']

        try:
            WebDriverWait(driver, timeout).until(EC.alert_is_present())
            Alert(driver).accept()
        except:
            pass

        try:
            accept = WebDriverWait(driver, timeout).until(EC.presence_of_element_located((By.CSS_SELECTOR, 'button#_evidon-accept-button')))
            accept = WebDriverWait(driver, timeout).until(EC.element_to_be_clickable((By.CSS_SELECTOR, 'button#_evidon-accept-button')))
            accept.click()
        except:
            pass


    def check_cities(self, response, city, timeout=5):
        driver = response.meta['driver']

        current_response = response.replace(body=str.encode(driver.page_source))
        partners = current_response.css('input[id="thePage:theForm:LocationData"]::attr(value)').extract_first()
        partners = partners.replace(r'\\"', r'\"')

        try:
            partners = json.loads(partners)
        except Exception as e:
            self.logger.error('Unable to parse city data: {}'.format(city), exc_info=True)
            self.logger.debug('{}'.format(partners))
            return False

        cities = [p['city'] for p in partners['locations']]
        _cities = [c for c in cities if city.lower() in c.lower()]

        if cities and not _cities:
            self.logger.warning('Unable to find right cities for the city: {} {}'.format(city, cities))
            return False
        else:
            self.logger.info('Cities are okay: {} {}'.format(city, cities))
            return True

