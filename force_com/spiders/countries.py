# -*- coding: utf-8 -*-
import re
import json
import time
import scrapy

from scrapy.shell import inspect_response

from ..scrapy_selenium import SeleniumRequest
from selenium.webdriver.common.by import By
from selenium.webdriver.common.alert import Alert
from selenium.webdriver.support.ui import Select
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC


class CountriesSpider(scrapy.Spider):
    name = 'countries'
    allowed_domains = ['dell.force.com']
    start_urls = ['http://dell.force.com/FAP_PartnerSearch']

    take_screenshot = False
    country_codes = []


    def start_requests(self):
        for url in self.start_urls:
            yield SeleniumRequest(url=url, callback=self.parse, wait_time=15, wait_until=EC.presence_of_element_located((By.CSS_SELECTOR, '[id="thePage:theForm:resultsTable"]')))


    def parse(self, response):
        # inspect_response(response, self)

        self.check_page(response)
        driver = response.meta['driver']

        if self.country_codes:
            country_codes = self.country_codes
        else:
            select = Select(driver.find_element_by_css_selector('select[id="thePage:theForm:searchComponent:searchFilterComponent:repeat:0:countryCode2"]'))
            current_option_value = select.first_selected_option.get_attribute('value')
            current_response = response.replace(body=str.encode(driver.page_source))
            options_values = current_response.css('select[id="thePage:theForm:searchComponent:searchFilterComponent:repeat:0:countryCode2"] option:not([disabled])::attr(value)').extract()
            options_values = sorted(list(set(options_values)))

            yield from self.parse_partner(response)
            country_codes = [v for v in options_values if v != current_option_value]

        for country_code in country_codes:
            if self.change_country(response, country_code):
                yield from self.parse_partner(response)
            else:
                self.logger.error('Unable to change country to {}'.format(country_code))


    def parse_partner(self, response, timeout=5):
        driver = response.meta['driver']

        try:
            select = Select(driver.find_element_by_css_selector('select[id="thePage:theForm:searchComponent:searchFilterComponent:repeat:0:countryCode2"]'))
            cc = select.first_selected_option.get_attribute('value')
        except Exception as e:
            self.logger.error('Something wrong', exc_info=True)
            return

        while True:
            current_response = response.replace(body=str.encode(driver.page_source))
            partners = current_response.css('input[id="thePage:theForm:LocationData"]::attr(value)').extract_first()
            partners = partners.replace(r'\\"', r'\"')

            try:
                partners = json.loads(partners)
            except Exception as e:
                self.logger.error('Unable to parse country data: {}'.format(cc), exc_info=True)
                self.logger.debug('{}'.format(partners))
                return

            partners = [{'url': 'http://dell.force.com/FAP_PartnerDetails?id={}'.format(p['id'].strip()), 'country': p['country'].strip()} for p in partners['locations']]

            yield from partners

            try:
                next_page = WebDriverWait(driver, timeout).until(EC.presence_of_element_located((By.CSS_SELECTOR, 'span[id="thePage:theForm:pageprev"] input[type="submit"]')))
                next_page = WebDriverWait(driver, timeout).until(EC.element_to_be_clickable((By.CSS_SELECTOR, 'span[id="thePage:theForm:pageprev"] input[type="submit"]')))
                driver.execute_script("return arguments[0].scrollIntoView(false);", next_page)
                driver.execute_script("window.scrollBy(0, 200);")
                next_page.click()
            except:
                self.logger.info('Breaking cycle', exc_info=True)
                if self.take_screenshot:
                    filename =  cc + '.png'
                    driver.save_screenshot(filename)
                break
            else:
                self.check_page(response)


    def change_country(self, response, country_code):
        self.logger.info('Changing country to: {}'.format(country_code))

        driver = response.meta['driver']

        select = Select(driver.find_element_by_css_selector('select[id="thePage:theForm:searchComponent:searchFilterComponent:repeat:0:countryCode2"]'))
        select.select_by_value(country_code)

        self.check_page(response)

        current_option_value = select.first_selected_option.get_attribute('value')

        if current_option_value == country_code:
            return True
        else:
            return False


    def check_page(self, response, timeout=5):
        driver = response.meta['driver']

        try:
            WebDriverWait(driver, timeout).until(EC.alert_is_present())
            Alert(driver).accept()
        except:
            pass

        try:
            accept = WebDriverWait(driver, timeout).until(EC.presence_of_element_located((By.CSS_SELECTOR, 'button#_evidon-accept-button')))
            accept = WebDriverWait(driver, timeout).until(EC.element_to_be_clickable((By.CSS_SELECTOR, 'button#_evidon-accept-button')))
            accept.click()
        except:
            pass
